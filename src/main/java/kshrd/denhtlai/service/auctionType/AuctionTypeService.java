package kshrd.denhtlai.service.auctionType;

import kshrd.denhtlai.model.AuctionType;

public interface AuctionTypeService {

    AuctionType findByAuctionTypeId(Integer auctionTypeId);

}
