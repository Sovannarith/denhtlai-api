package kshrd.denhtlai.service.auctionType;

import kshrd.denhtlai.model.AuctionType;
import kshrd.denhtlai.repository.auction.AuctionRepository;
import kshrd.denhtlai.repository.auctionType.AuctionTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuctionTypeServiceImpl implements AuctionTypeService {

    private AuctionTypeRepository auctionTypRepository;

    @Autowired
    AuctionTypeServiceImpl(AuctionTypeRepository auctionTypRepository){
        this.auctionTypRepository = auctionTypRepository;

    }

    @Override
    public AuctionType findByAuctionTypeId(Integer auctionTypeId) {
        return auctionTypRepository.findByAuctionTypeId(auctionTypeId);
    }
}
