package kshrd.denhtlai.service.topUp;

import kshrd.denhtlai.model.TopUp;
import org.springframework.data.domain.Page;

import java.util.List;

public interface TopUpService {

    TopUp topUp(TopUp topUp);

    TopUp topUp_(Integer userId, String currency, double amount, Integer payment_method);

    Page<TopUp> findTopUps(Integer pageNumber, Integer pageSize, String order, String field);

    Page<TopUp> findById(Integer id, Integer pageNumber, Integer pageSize, String sort, String field);

    Page<TopUp> findByUserId(Integer userId, Integer pageNumber, Integer pageSize, String sort, String field);

    Page<TopUp> findByPaymentMethod_PaymentMethodId(Integer id, Integer pageNumber, Integer pageSize, String sort, String field);
}
