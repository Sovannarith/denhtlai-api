package kshrd.denhtlai.service.topUp;

import kshrd.denhtlai.model.PaymentMethod;
import kshrd.denhtlai.model.TopUp;
import kshrd.denhtlai.model.User;
import kshrd.denhtlai.repository.topUp.TopUpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TopUpServiceImpl implements TopUpService {

    private TopUpRepository topUpRepository;

    @Autowired
    public TopUpServiceImpl(TopUpRepository topUpRepository){
        this.topUpRepository = topUpRepository;
    }

    @Override
    public TopUp topUp(TopUp topUp) {
        return topUpRepository.save(topUp);
    }

    @Override
    public Page<TopUp> findTopUps(Integer pageNumber, Integer pageSize, String sort, String field) {

        String fields[] = {"currency", "amount", "topUpDate", "topUpId"};

        String fieldName = "";
        pageNumber = pageNumber == null ? 0 : pageNumber >= 0 ? pageNumber : 0;
        pageSize = pageSize == null ? 10 : pageSize > 0 ? pageSize : 1;
        sort = sort == null ? "ASC" : sort;
        Sort.Direction direction =  sort.equalsIgnoreCase("DESC") ? Sort.Direction.DESC : Sort.DEFAULT_DIRECTION;
        field = field == null ? "" : field;
        for (String f: fields) {
            if(field.toUpperCase().equalsIgnoreCase(f.toUpperCase())){
                fieldName = field; break;
            }else  fieldName = fields[0];
        }
        return topUpRepository.findAll(new PageRequest(pageNumber, pageSize, new Sort(direction, fieldName)));
    }

    @Override
    public TopUp topUp_(Integer userId, String currency, double amount, Integer payment_method) {

        String[] arrCurrency = {"kh", "us"};
        Integer[] arrPaymentMethod = {1, 2};

        if(userId == null) return null;
        currency = currency.toUpperCase() == arrCurrency[0].toUpperCase() ? arrCurrency[0] : arrCurrency[1];
        payment_method = payment_method == arrPaymentMethod[0] ? arrPaymentMethod[0] : arrPaymentMethod[1];
        amount = amount > 0 ? amount : 0.00;

        TopUp topUp = new TopUp();
            PaymentMethod paymentMethod = new PaymentMethod();
                paymentMethod.setPaymentMethodId(payment_method);
            User user = new User();
                user.setUserId(userId);
            topUp.setAdminId(null);
            topUp.setCurrency(currency);
            topUp.setAmount(amount);
            topUp.setPaymentMethod(paymentMethod);
            topUp.setUser(user);
        return topUpRepository.save(topUp);
    }

    @Override
    public Page<TopUp> findById(Integer topUpId, Integer pageNumber, Integer pageSize, String sort, String field) {

        String fields[] = {"topUpId","currency", "amount", "topUpDate"};
        String fieldName = "";
        pageNumber = pageNumber == null ? 0 : pageNumber >= 0 ? pageNumber : 0;
        pageSize = pageSize == null ? 10 : pageSize > 0 ? pageSize : 1;
        sort = sort == null ? "ASC" : sort;
        Sort.Direction direction = sort.toUpperCase() == "DESC" ? Sort.Direction.DESC : Sort.DEFAULT_DIRECTION;
        field = field == null ? "" : field;
        for (String f: fields) {
            if (field.toUpperCase().equalsIgnoreCase(f.toUpperCase())) {
                fieldName = f;
                break;
            } else fieldName = fields[0];
        }
        return topUpRepository.findByTopUpId(topUpId,  new PageRequest(pageNumber, pageSize, new Sort(direction, fieldName)));
    }

    @Override
    public Page<TopUp> findByUserId(Integer userId, Integer pageNumber, Integer pageSize, String sort, String field) {

        String fields[] = {"topUpId","currency", "amount", "topUpDate"};
        String fieldName = "";
        pageNumber = pageNumber == null ? 0 : pageNumber >= 0 ? pageNumber : 0;
        pageSize = pageSize == null ? 10 : pageSize > 0 ? pageSize : 1;
        sort = sort == null ? "ASC" : sort;
        Sort.Direction direction = sort.toUpperCase() == "DESC" ? Sort.Direction.DESC : Sort.DEFAULT_DIRECTION;
        field = field == null ? "" : field;
        for (String f: fields) {
            if(field.toUpperCase().equalsIgnoreCase(f.toUpperCase())){
                fieldName = f; break;
            }else  fieldName = fields[0];
        }

        return topUpRepository.findByUser_UserId(userId, new PageRequest(pageNumber, pageSize, new Sort(direction, fieldName)));
    }

    @Override
    public Page<TopUp> findByPaymentMethod_PaymentMethodId(Integer paymentMethodId, Integer pageNumber, Integer pageSize, String sort, String field) {

        String fields[] = {"topUpId","currency", "amount", "topUpDate"};
        String fieldName = "";
        pageNumber = pageNumber == null ? 0 : pageNumber >= 0 ? pageNumber : 0;
        pageSize = pageSize == null ? 10 : pageSize > 0 ? pageSize : 1;
        sort = sort == null ? "ASC" : sort;
        Sort.Direction direction = sort.toUpperCase() == "DESC" ? Sort.Direction.DESC : Sort.DEFAULT_DIRECTION;
        field = field == null ? "" : field;
        for (String f: fields) {
            if(field.toUpperCase().equalsIgnoreCase(f.toUpperCase())){
                fieldName = f; break;
            }else  fieldName = fields[0];
        }

        return topUpRepository.findByPaymentMethod_PaymentMethodId(paymentMethodId, new PageRequest(pageNumber, pageSize, new Sort(direction, fieldName)));
    }

}
