package kshrd.denhtlai.service.category;

import kshrd.denhtlai.model.Category;
import kshrd.denhtlai.repository.category.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository){
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Page<Category> findAll(Integer pageNumber, Integer pageSize, String sort, String field) {
        String fields[] = {"categoryId", "name", "parentId"};

        String fieldName = "";
        pageNumber = pageNumber == null ? 0 : pageNumber >= 0 ? pageNumber : 0;
        pageSize = pageSize == null ? 10 : pageSize > 0 ? pageSize : 1;
        sort = sort == null ? "ASC" : sort;
        Sort.Direction direction = sort.equalsIgnoreCase("DESC") ? Sort.Direction.DESC : Sort.DEFAULT_DIRECTION;
        field = field == null ? "" : field;
        for (String f: fields) {
            if(field.toUpperCase().equalsIgnoreCase(f.toUpperCase())){
                fieldName = field; break;
            }else  fieldName = fields[0];
        }
        return categoryRepository.findAll(new PageRequest(pageNumber, pageSize, new Sort(direction, fieldName)));
    }

    @Override
    public Category save(Category category) {
        Category category1  =  categoryRepository.findOne(category.getParentId().getCategoryId());
        if (category1 != null) category1 = categoryRepository.save(category);
        return category1;
    }

    @Override
    public Category update(Integer id, Category category) {
        Category c = categoryRepository.findOne(id);
        if (category.getName() != null) c.setName(category.getName());
        if (category.getParentId() != null){
            c.setParentId(category.getParentId());
        }
        return categoryRepository.save(c);
    }

    @Override
    public Category delete(Integer id) {
        Category c = categoryRepository.findOne(id);
        if(c != null) categoryRepository.delete(c);
        return c;
    }

    @Override
    public Category findById(Integer id) {
        return categoryRepository.findByCategoryId(id);
    }
}
