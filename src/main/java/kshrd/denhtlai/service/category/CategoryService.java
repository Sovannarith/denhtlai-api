package kshrd.denhtlai.service.category;

import kshrd.denhtlai.model.Category;
import org.springframework.data.domain.Page;


public interface CategoryService {

    Page<Category> findAll(Integer pageNumber, Integer pageSize, String sort, String field);

    Category save(Category category);

    Category update(Integer id, Category category);

    Category delete(Integer id);

    Category findById(Integer id);

}
