package kshrd.denhtlai.service.product;

import kshrd.denhtlai.model.Product;
import org.springframework.data.domain.Page;

public interface ProductService {

    Product save(Product product);

    Product update(Integer id, Product product);

    Page<Product> findAll(Integer pageNumber, Integer pageSize, String sort, String field);

    Product delete(Integer id);

    Product findById(Integer id);

}
