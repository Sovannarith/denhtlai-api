package kshrd.denhtlai.service.product;

import kshrd.denhtlai.model.Product;
import kshrd.denhtlai.repository.product.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;

    @Autowired
    ProductServiceImpl( ProductRepository productRepository){
        this.productRepository = productRepository;
    }

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    public Product update(Integer id, Product product) {
        Product p = productRepository.findOne(id);
        if(p != null) p = productRepository.save(product);
        return p;
    }

    @Override
    public Product findById(Integer id) {
        return productRepository.findByProId(id);
    }

    @Override
    public Page<Product> findAll(Integer pageNumber, Integer pageSize, String sort, String field) {
        String fields[] = {"proId","name"};
        String fieldName = "";
        pageNumber = pageNumber == null ? 0 : pageNumber >= 0 ? pageNumber : 0;
        pageSize = pageSize == null ? 10 : pageSize > 0 ? pageSize : 1;
        sort = sort == null ? "ASC" : sort;
        Sort.Direction direction = sort.equalsIgnoreCase("DESC") ? Sort.Direction.DESC : Sort.DEFAULT_DIRECTION;
        field = field == null ? "" : field;
        for (String f: fields) {
            if(field.toUpperCase().equalsIgnoreCase(f.toUpperCase())){
                fieldName = f; break;
            }else  fieldName = fields[0];
        }
        return productRepository.findAll(new PageRequest(pageNumber, pageSize, new Sort(direction, fieldName)));
    }

    @Override
    public Product delete(Integer id) {
        Product p = productRepository.findOne(id);
        if(p != null) productRepository.delete(p);
        return p;
    }
}
