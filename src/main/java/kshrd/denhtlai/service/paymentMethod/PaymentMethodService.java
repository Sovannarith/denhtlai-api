package kshrd.denhtlai.service.paymentMethod;

import kshrd.denhtlai.model.PaymentMethod;

public interface PaymentMethodService {

    PaymentMethod getPaymentMethodById(Integer id);

}
