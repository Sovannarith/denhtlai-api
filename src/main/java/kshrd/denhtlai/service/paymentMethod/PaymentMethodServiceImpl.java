package kshrd.denhtlai.service.paymentMethod;

import kshrd.denhtlai.model.PaymentMethod;
import kshrd.denhtlai.repository.paymentMethod.PaymentMethodRepository;
import org.springframework.stereotype.Service;

@Service
public class PaymentMethodServiceImpl implements PaymentMethodService{

    private PaymentMethodRepository paymentMethodRepository;

    public PaymentMethodServiceImpl(PaymentMethodRepository paymentMethodRepository){
        this.paymentMethodRepository = paymentMethodRepository;
    }

    @Override
    public PaymentMethod getPaymentMethodById(Integer id) {
        return paymentMethodRepository.findByPaymentMethodId(id);
    }
}
