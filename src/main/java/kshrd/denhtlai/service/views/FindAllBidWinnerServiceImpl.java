package kshrd.denhtlai.service.views;

import kshrd.denhtlai.model.views.FindAllBidWinner;
import kshrd.denhtlai.repository.views.FindAllBidWinnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class FindAllBidWinnerServiceImpl implements FindAllBidWinnerService {

    @Autowired
    FindAllBidWinnerRepository findAllBidWinnerRepository;

    @Override
    public Page<FindAllBidWinner> findAll(Integer pageNumber, Integer pageSize, String sort, String field) {
        String fields[] = {"auctionId", "productName", "firstName", "lastName", "phone", "email", "currentPrice", "bidDate"};
            String fieldName = "";
            pageNumber = pageNumber == null ? 0 : pageNumber >= 0 ? pageNumber : 0;
            pageSize = pageSize == null ? 10 : pageSize > 0 ? pageSize : 1;
            sort = sort == null ? "ASC" : sort;
            Sort.Direction direction = sort.equalsIgnoreCase("DESC") ? Sort.Direction.DESC : Sort.DEFAULT_DIRECTION;
            field = field == null ? "" : field;
            for (String f: fields) {
                if(f.toUpperCase().equalsIgnoreCase(field.toUpperCase())){
                    fieldName = f; break;
                }else  fieldName = fields[0];
            }
        return findAllBidWinnerRepository.findAll(new PageRequest(pageNumber, pageSize, new Sort(direction, fieldName)));
    }
}
