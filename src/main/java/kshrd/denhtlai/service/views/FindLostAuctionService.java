package kshrd.denhtlai.service.views;

import kshrd.denhtlai.model.views.FindLostAuction;
import org.springframework.data.domain.Page;

import java.awt.print.Pageable;

public interface FindLostAuctionService {

    Page<FindLostAuction> findByWUserIdNotAndUserIdIs(Integer userId, Integer pageNumber, Integer pageSize, String sort, String field);

}
