package kshrd.denhtlai.service.views;

import kshrd.denhtlai.model.views.FindAllBidWinner;
import org.springframework.data.domain.Page;

public interface FindAllBidWinnerService {

    Page<FindAllBidWinner> findAll(Integer pageNumber, Integer pageSize, String sort, String field);
}
