package kshrd.denhtlai.service.views;

import kshrd.denhtlai.model.views.FindLostAuction;
import kshrd.denhtlai.repository.views.FindLostAuctionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FindLostAuctionServiceImpl implements FindLostAuctionService{

    private FindLostAuctionRepository findLostAuctionRepository;

    @Autowired
    public FindLostAuctionServiceImpl(FindLostAuctionRepository findLostAuctionRepository){
        this.findLostAuctionRepository = findLostAuctionRepository;
    }


    @Override
    public Page<FindLostAuction> findByWUserIdNotAndUserIdIs(Integer userId, Integer pageNumber, Integer pageSize, String sort, String field) {

        String fields[] = {"wBidId", "userId", "wAuctionId", "wPrice", "wBidDate", "losePrice", "productName"};
            String fieldName = "";
            pageNumber = pageNumber == null ? 0 : pageNumber >= 0 ? pageNumber : 0;
            pageSize = pageSize == null ? 10 : pageSize > 0 ? pageSize : 1;
            sort = sort == null ? "ASC" : sort;
            Sort.Direction direction =  sort.equalsIgnoreCase("DESC") ? Sort.Direction.DESC : Sort.DEFAULT_DIRECTION;
            field = field == null ? "" : field;
            for (String f: fields) {
                if(field.toUpperCase().equalsIgnoreCase(f.toUpperCase())){
                    fieldName = f; break;
                }else  fieldName = fields[0];
            }
        return findLostAuctionRepository.findByWUserIdNotAndUserIdIs(userId, userId, new PageRequest(pageNumber, pageSize, new Sort(direction, fieldName)));
    }
}
