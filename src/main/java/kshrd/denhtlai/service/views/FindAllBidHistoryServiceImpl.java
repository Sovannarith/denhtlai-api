package kshrd.denhtlai.service.views;

import kshrd.denhtlai.model.views.FindAllBidHistory;
import kshrd.denhtlai.repository.views.FindAllBidHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class FindAllBidHistoryServiceImpl implements FindAllBidHistoryService {

    @Autowired
    private FindAllBidHistoryRepository findAllBidHistoryRepository;

    @Override
    public Page<FindAllBidHistory> findAll(Integer pageNumber, Integer pageSize, String sort, String field) {
        String fields[] = {"bidId", "auctionId", "productName", "firstName", "lastName", "bidPrice", "bidDate"};
            String fieldName = "";
            pageNumber = pageNumber == null ? 0 : pageNumber >= 0 ? pageNumber : 0;
            pageSize = pageSize == null ? 10 : pageSize > 0 ? pageSize : 1;
            sort = sort == null ? "ASC" : sort;
            Sort.Direction direction = sort.equalsIgnoreCase("DESC") ? Sort.Direction.DESC : Sort.DEFAULT_DIRECTION;
            field = field == null ? "" : field;
            for (String f: fields) {
                if(f.toUpperCase().equalsIgnoreCase(field.toUpperCase())){
                    fieldName = f; break;
                }else  fieldName = fields[0];
            }
        return findAllBidHistoryRepository.findAll(new PageRequest(pageNumber, pageSize, new Sort(direction, fieldName)));
    }
}
