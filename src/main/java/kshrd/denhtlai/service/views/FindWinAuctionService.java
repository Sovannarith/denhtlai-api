package kshrd.denhtlai.service.views;

import kshrd.denhtlai.model.views.FindWinAuction;
import org.springframework.data.domain.Page;

public interface FindWinAuctionService {

    Page<FindWinAuction> findByUserId(Integer userId, Integer pageNumber, Integer pageSize, String sort, String field);

}
