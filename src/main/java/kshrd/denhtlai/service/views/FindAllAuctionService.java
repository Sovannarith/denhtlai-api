package kshrd.denhtlai.service.views;

import kshrd.denhtlai.model.views.FindAllAuctions;
import kshrd.denhtlai.repository.views.FindAllAuctionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.util.List;

public interface FindAllAuctionService {

    Page<FindAllAuctions> findAll(Integer pageNumber, Integer pageSize, String sort, String field);


    FindAllAuctions findByAuctionId(Integer auctionId);

}
