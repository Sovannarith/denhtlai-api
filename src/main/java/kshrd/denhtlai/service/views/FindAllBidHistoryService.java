package kshrd.denhtlai.service.views;

import kshrd.denhtlai.model.views.FindAllBidHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface FindAllBidHistoryService {

    Page<FindAllBidHistory> findAll(Integer pageNumber, Integer pageSize, String sort, String field);

}
