package kshrd.denhtlai.service.views;

import kshrd.denhtlai.model.views.FindLostAuction;
import kshrd.denhtlai.model.views.FindWinAuction;
import kshrd.denhtlai.repository.views.FindWinAuctionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FindWinAuctionServiceImpl implements FindWinAuctionService {

    private FindWinAuctionRepository findWinAuctionRepository;

    @Autowired
    public FindWinAuctionServiceImpl(FindWinAuctionRepository findWinAuctionRepository){
        this.findWinAuctionRepository = findWinAuctionRepository;
    }

    @Override
    public Page<FindWinAuction> findByUserId(Integer userId, Integer pageNumber, Integer pageSize, String sort, String field) {

        String fields[] = {"auctionId", "productName", "userId", "bidDate", "currentPrice", "status"};
        String fieldName = "";
        pageNumber = pageNumber == null ? 0 : pageNumber >= 0 ? pageNumber : 0;
        pageSize = pageSize == null ? 10 : pageSize > 0 ? pageSize : 1;
        sort = sort == null ? "ASC" : sort;
        Sort.Direction direction = sort.equalsIgnoreCase("DESC") ? Sort.Direction.DESC : Sort.DEFAULT_DIRECTION;
        field = field == null ? "" : field;
        for (String f: fields) {
            if(field.toUpperCase().equalsIgnoreCase(f.toUpperCase())){
                fieldName = f; break;
            }else  fieldName = fields[0];
        }

        return findWinAuctionRepository.findByUserId(userId, new PageRequest(pageNumber, pageSize, new Sort(direction, fieldName)));
    }
}
