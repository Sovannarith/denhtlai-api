package kshrd.denhtlai.service.views;

import kshrd.denhtlai.model.views.FindAllAuctions;
import kshrd.denhtlai.repository.views.FindAllAuctionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FindAllAuctionServiceImpl implements FindAllAuctionService {

    @Autowired
    FindAllAuctionRepository findAllAuctionRepository;

    @Override
    public Page<FindAllAuctions> findAll(Integer pageNumber, Integer pageSize, String sort, String field) {
        String fields[] = {"auctionId", "productName", "categoryName", "totalBid", "buyPrice", "startDate", "endDate", "incrementPrice"};
            String fieldName = "";
            pageNumber = pageNumber == null ? 0 : pageNumber >= 0 ? pageNumber : 0;
            pageSize = pageSize == null ? 10 : pageSize > 0 ? pageSize : 1;
            sort = sort == null ? "ASC" : sort;
            Sort.Direction direction = sort.equalsIgnoreCase("DESC") ? Sort.Direction.DESC : Sort.DEFAULT_DIRECTION;
            field = field == null ? "" : field;
            for (String f: fields) {
                if(field.toUpperCase().equalsIgnoreCase(f.toUpperCase())){
                    fieldName = f; break;
                }else  fieldName = fields[0];
            }
        return findAllAuctionRepository.findAll(new PageRequest(pageNumber, pageSize, new Sort(direction, fieldName)));
    }

    @Override
    public FindAllAuctions findByAuctionId(Integer auctionId) {
        return findAllAuctionRepository.findByAuctionId(auctionId);
    }
}
