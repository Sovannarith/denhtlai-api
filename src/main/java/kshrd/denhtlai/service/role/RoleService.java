package kshrd.denhtlai.service.role;

import kshrd.denhtlai.model.Role;

public interface RoleService {

    Role findByRoleId(Integer id);

}
