package kshrd.denhtlai.service.role;

import kshrd.denhtlai.model.Role;
import kshrd.denhtlai.repository.role.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    private RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository){
        this.roleRepository = roleRepository;
    }


    @Override
    public Role findByRoleId(Integer id) {
        return roleRepository.findByRoleId(id);
    }
}
