package kshrd.denhtlai.service.auction;

import kshrd.denhtlai.model.Auction;
import kshrd.denhtlai.repository.auction.AuctionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class AuctionServiceImpl implements AuctionService{

    private AuctionRepository auctionRepository;

    @Autowired
    public AuctionServiceImpl(AuctionRepository auctionRepository){
        this.auctionRepository = auctionRepository;
    }

    @Override
    public Auction save(Auction auction) {
        Auction auction1 = null;
        int count = 0;
        while (auction1 == null){
            try {
                count++;
                auction1 = auctionRepository.save(auction);
            }catch (Exception e){
                break;
            }
                System.out.println("AUCTION "+auction1 + " : " + count);
        }

        return auction1;
    }

    @Override
    public Auction update(Auction auction) {
        Auction a = auctionRepository.findOne(auction.getAuctionId());
        if(a != null) a = auctionRepository.save(auction);
        return a;
    }

    @Override
    public Auction findById(Integer id) {
        return auctionRepository.findOne(id);
    }

    @Override
    public Page<Auction> findByProductNameIgnoreCaseContains(String productName, Integer pageNumber, Integer pageSize, String sort, String field) {

        String fields[] = {"auctionId", "productName", "categoryName", "totalBid", "buyPrice", "startDate", "endDate", "incrementPrice"};
        String fieldName = "";
        pageNumber = pageNumber == null ? 0 : pageNumber >= 0 ? pageNumber : 0;
        pageSize = pageSize == null ? 10 : pageSize > 0 ? pageSize : 1;
        sort = sort == null ? "ASC" : sort;
        Sort.Direction direction = sort.equalsIgnoreCase("DESC") ? Sort.Direction.DESC : Sort.DEFAULT_DIRECTION;
        field = field == null ? "" : field;
        for (String f: fields) {
            if(field.toUpperCase().equalsIgnoreCase(f.toUpperCase())){
                fieldName = f; break;
            }else  fieldName = fields[0];
        }

        return auctionRepository.findByProductNameIgnoreCaseContains(productName, new PageRequest(pageNumber,pageSize, new Sort(direction, fieldName)));
    }

    @Override
    public Page<Auction> findAll(Integer pageNumber, Integer pageSize, String sort, String field) {
        String fields[] = {"auctionId", "productName", "description", "currentPrice", "buyPrice", "startDate", "endDate", "incrementPrice"};
        String fieldName = "";
        pageNumber = pageNumber == null ? 0 : pageNumber >= 0 ? pageNumber : 0;
        pageSize = pageSize == null ? 10 : pageSize > 0 ? pageSize : 1;
        sort = sort == null ? "ASC" : sort;
        Sort.Direction direction = sort.equalsIgnoreCase("DESC") ? Sort.Direction.DESC : Sort.DEFAULT_DIRECTION;
        field = field == null ? "" : field;
        for (String f: fields) {
            if(field.toUpperCase().equalsIgnoreCase(f.toUpperCase())){
                fieldName = f; break;
            }else  fieldName = fields[0];
        }

        return auctionRepository.findAll(new PageRequest(pageNumber,pageSize, new Sort(direction, fieldName)));
    }

    @Override
    public Page<Auction> findByUser_UserId(Integer userId, Integer pageNumber, Integer pageSize, String sort, String field) {
        String fields[] = {"auctionId", "productName", "description", "currentPrice", "buyPrice", "startDate", "endDate", "incrementPrice"};
        String fieldName = "";
        pageNumber = pageNumber == null ? 0 : pageNumber >= 0 ? pageNumber : 0;
        pageSize = pageSize == null ? 10 : pageSize > 0 ? pageSize : 1;
        sort = sort == null ? "ASC" : sort;
        Sort.Direction direction = sort.equalsIgnoreCase("DESC") ? Sort.Direction.DESC : Sort.DEFAULT_DIRECTION;
        field = field == null ? "" : field;
        for (String f: fields) {
            if(field.toUpperCase().equalsIgnoreCase(f.toUpperCase())){
                fieldName = f; break;
            }else  fieldName = fields[0];
        }

        return auctionRepository.findByUser_UserId(userId, new PageRequest(pageNumber,pageSize, new Sort(direction, fieldName)));
    }
}
