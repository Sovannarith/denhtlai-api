package kshrd.denhtlai.service.auction;

import kshrd.denhtlai.model.Auction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface AuctionService {


    Auction save(Auction auction);

    Auction update(Auction auction);

    Auction findById(Integer id);

    Page<Auction> findByProductNameIgnoreCaseContains(String productName, Integer pageNumber, Integer pageSize, String sort, String field);

    Page<Auction> findAll(Integer pageNumber, Integer pageSize, String sort, String field);

    Page<Auction> findByUser_UserId(Integer userId, Integer pageNumber, Integer pageSize, String sort, String field);

}
