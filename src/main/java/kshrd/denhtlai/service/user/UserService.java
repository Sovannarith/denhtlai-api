package kshrd.denhtlai.service.user;

import kshrd.denhtlai.model.User;
import org.springframework.data.domain.Page;

public interface UserService {

    Page<User> findAll(Integer pageNumber, Integer pageSize, String sort, String field);

    User findById(Integer id);

    User findByPhoneAndUid(String phone, String uid);

    User save(User user);

    Page<User> findByFirstNameIgnoreCaseContainsOrLastNameIgnoreCaseContains(String firstName, String lastName, Integer pageNumber, Integer pageSize, String sort, String field);

    User update(Integer userId, User user);

    User adminUpdate(Integer userId, User user);

}
