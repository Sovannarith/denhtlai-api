package kshrd.denhtlai.service.user;

import kshrd.denhtlai.model.User;
import kshrd.denhtlai.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public Page<User> findAll(Integer pageNumber, Integer pageSize, String sort, String field) {

        String fields[] = {"firstName", "lastName", "gender", "phone", "email"};
        String fieldName = "";
        pageNumber = pageNumber == null ? 0 : pageNumber >= 0 ? pageNumber : 0;
        pageSize = pageSize == null ? 10 : pageSize > 0 ? pageSize : 1;
        sort = sort == null ? "ASC" : sort;
        Sort.Direction direction = sort.toUpperCase() == "DESC" ? Sort.Direction.DESC : Sort.DEFAULT_DIRECTION;
        field = field == null ? "" : field;
        for (String f: fields) {
            if(field.toUpperCase().equalsIgnoreCase(f.toUpperCase())){
                fieldName = f; break;
            }else  fieldName = fields[0];
        }
        return userRepository.findAll(new PageRequest(pageNumber, pageSize, new Sort(direction, fieldName)));
    }

    @Override
    public User findById(Integer id) {
        return userRepository.findByUserId(id);
    }

    @Override
    public User findByPhoneAndUid(String phone, String uid) {
        return userRepository.findByPhoneAndUid(phone, uid);
    }

    @Override
    public User save(User user) {
        userRepository.save(user);
        return user;
    }

    @Override
    public Page<User> findByFirstNameIgnoreCaseContainsOrLastNameIgnoreCaseContains(String firstName, String lastName, Integer pageNumber, Integer pageSize, String sort, String field) {
        String fields[] = {"firstName", "lastName", "gender", "phone", "email"};

        String fieldName = "";
        pageNumber = pageNumber == null ? 0 : pageNumber >= 0 ? pageNumber : 0;
        pageSize = pageSize == null ? 10 : pageSize > 0 ? pageSize : 1;
        sort = sort == null ? "ASC" : sort;
        Sort.Direction direction = sort.equalsIgnoreCase("DESC") ? Sort.Direction.DESC : Sort.DEFAULT_DIRECTION;
        field = field == null ? "" : field;
        for (String f: fields) {
            if(field.toUpperCase().equalsIgnoreCase(f.toUpperCase())){
                fieldName = field; break;
            }else  fieldName = fields[0];
        }
        return userRepository.findByFirstNameIgnoreCaseContainsOrLastNameIgnoreCaseContains(firstName, lastName, new PageRequest(pageNumber, pageSize, new Sort(direction, fieldName)));
    }

    @Override
    public User update(Integer userId, User user) {
        User u = userRepository.findOne(userId);
        if(!user.getFirstName().equals("")){
            u.setFirstName(user.getFirstName());
        }
        if(user.getLastName() != null && user.getLastName() != ""){
            u.setLastName(user.getLastName());
        }
        if(!user.getEmail().equals(null) && !user.getEmail().equals("")){
            u.setEmail(user.getEmail());
        }
        if(!user.getGender().equals(null) && !user.getGender().equals("")){
            u.setGender(user.getGender());
        }
        return userRepository.save(u);
    }

    @Override
    public User adminUpdate(Integer userId, User user) {
        User u = userRepository.findOne(userId);
        if(!user.getFirstName().equals("")){
            u.setFirstName(user.getFirstName());
        }
        if(user.getLastName() != null && user.getLastName() != ""){
            u.setLastName(user.getLastName());
        }
        if(!user.getEmail().equals(null) && !user.getEmail().equals("")){
            u.setEmail(user.getEmail());
        }
        if(!user.getGender().equals(null) && !user.getGender().equals("")){
            u.setGender(user.getGender());
        }
        if(user.getRole() != null) u.setRole(user.getRole());
        if(user.getAuctions() != null) u.setAuctions(user.getAuctions());
        if(user.getBidHistoryList() != null) u.setBidHistoryList(user.getBidHistoryList());
        if(user.getInvoicePayees() != null) u.setInvoicePayees(user.getInvoicePayees());
        if(user.getInvoices() != null) u.setInvoices(user.getInvoices());
        if(user.getAuctions() != null) u.setAuctions(user.getAuctions());
        if(!user.getPhone().equals(null) && !user.getPhone().equals("")) u.setPhone(user.getPhone());
        if(!user.getPassword().equals(null) && !user.getPassword().equals("")) u.setPassword(user.getPassword());
        if(!user.getPhoto().equals(null) && !user.getPhoto().equals("")) u.setPhoto(user.getPhoto());
        if(!user.getUserCreditHistories().equals(null)) u.setUserCreditHistories(user.getUserCreditHistories());
        if(!user.getStatus().equals(null) && !user.getStatus().equals("")) u.setStatus(user.getStatus());
        if(!user.getWingTopUps().equals(null)) u.setWingTopUps(user.getWingTopUps());
        return userRepository.save(u);
    }


}
