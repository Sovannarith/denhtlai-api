package kshrd.denhtlai.repository.topUp;

import kshrd.denhtlai.model.TopUp;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TopUpRepository extends JpaRepository<TopUp, Integer> {

    Page<TopUp> findByPaymentMethod_PaymentMethodId(Integer id, Pageable pageable);

    Page<TopUp> findByUser_UserId(Integer userId, Pageable pageable);

    Page<TopUp> findByTopUpId(Integer topUpId, Pageable pageable);

}
