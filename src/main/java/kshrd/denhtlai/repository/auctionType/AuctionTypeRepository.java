package kshrd.denhtlai.repository.auctionType;

import kshrd.denhtlai.model.AuctionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuctionTypeRepository extends JpaRepository<AuctionType,Integer>{

    AuctionType findByAuctionTypeId(Integer auctionTypeId);

}
