package kshrd.denhtlai.repository.views;

import kshrd.denhtlai.model.views.FindAllBidHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FindAllBidHistoryRepository extends JpaRepository<FindAllBidHistory, Integer> {

    Page<FindAllBidHistory> findAll(Pageable pageable);

}
