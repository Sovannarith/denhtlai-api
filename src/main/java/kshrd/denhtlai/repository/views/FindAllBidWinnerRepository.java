package kshrd.denhtlai.repository.views;

import kshrd.denhtlai.model.views.FindAllBidWinner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FindAllBidWinnerRepository extends JpaRepository<FindAllBidWinner, Integer> {

}
