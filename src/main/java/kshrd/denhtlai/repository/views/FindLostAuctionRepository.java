package kshrd.denhtlai.repository.views;

import kshrd.denhtlai.model.views.FindLostAuction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FindLostAuctionRepository extends JpaRepository<FindLostAuction, Integer> {

    Page<FindLostAuction> findByWUserIdNotAndUserIdIs(Integer wUserId, Integer userId, Pageable pageable);

}
