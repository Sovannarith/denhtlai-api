package kshrd.denhtlai.repository.views;

import kshrd.denhtlai.model.views.FindAllAuctions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FindAllAuctionRepository extends JpaRepository<FindAllAuctions, Integer>{

    Page<FindAllAuctions> findAll(Pageable pageable);

    FindAllAuctions findByAuctionId(Integer auctionId);

}
