package kshrd.denhtlai.repository.views;

import kshrd.denhtlai.model.views.FindWinAuction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FindWinAuctionRepository extends JpaRepository<FindWinAuction, Integer> {

    public Page<FindWinAuction> findByUserId(Integer userId, Pageable pageable);

}
