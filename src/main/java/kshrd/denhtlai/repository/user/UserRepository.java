package kshrd.denhtlai.repository.user;

import kshrd.denhtlai.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

    List<User> findAll();

    User findByPhoneAndUid(String phone, String uid);

    User findByUserId(Integer id);

    Page<User> findByFirstNameIgnoreCaseContainsOrLastNameIgnoreCaseContains(String firstName, String lastName, Pageable pageable);

    Page<User> findAll(Pageable pageable);

}
