package kshrd.denhtlai.repository.paymentMethod;

import kshrd.denhtlai.model.PaymentMethod;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentMethodRepository extends JpaRepository<PaymentMethod, Integer>{

    PaymentMethod findByPaymentMethodId(Integer id);

}
