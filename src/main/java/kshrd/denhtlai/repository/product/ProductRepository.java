package kshrd.denhtlai.repository.product;

import kshrd.denhtlai.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    Product findByProId(Integer id);

}
