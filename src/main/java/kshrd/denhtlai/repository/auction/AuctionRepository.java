package kshrd.denhtlai.repository.auction;

import kshrd.denhtlai.model.Auction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuctionRepository extends JpaRepository<Auction, Integer> {

    Page<Auction> findByProductNameIgnoreCaseContains(String productName, Pageable pageable);

    Page<Auction> findByUser_UserId(Integer userId, Pageable pageable);

    Page<Auction> findAll(Pageable pageable);
}
