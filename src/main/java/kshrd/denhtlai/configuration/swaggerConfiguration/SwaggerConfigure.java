package kshrd.denhtlai.configuration.swaggerConfiguration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
@EnableJpaAuditing
public class SwaggerConfigure {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("kshrd.denhtlai.controller.rest"))
                .paths(PathSelectors.ant("/**"))
//                .apis(RequestHandlerSelectors.any())
                .build().apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "My REST API",
                "Some custom description of API.",
                "API DenhTlai",
                "Terms of service",
                new Contact("KSHRD Center", "www.kshrd.com.kh", "cheavsovannarith@gmail.com"),
                "License of API", "API license URL", Collections.emptyList());
    }
}
