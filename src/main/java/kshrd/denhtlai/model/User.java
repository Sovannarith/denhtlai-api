package kshrd.denhtlai.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "dt_users")
public class User extends ResourceSupport implements Serializable{

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Integer userId;

    //foreign key of Role
    @ManyToOne
    @JoinColumn(name = "role_id")
    //@JsonManagedReference
    private Role role;

    // Not a column in table, just a relationship with Entity Auction
    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Auction> auctions;

    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<UserCreditHistory> userCreditHistories;

    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<WingTopUp> wingTopUps;

    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Invoice> invoices;

    @JsonIgnore
    @OneToMany(mappedBy = "payee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Invoice> invoicePayees;

    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<BidHistory> bidHistoryList;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    private String gender;

    private String email;

    private String phone;

    private String photo;

    private String status;

    @Column(name = "register_date")
//    @CreatedDate
//    @Temporal(TemporalType.TIMESTAMP)
    @Temporal(TemporalType.DATE)
    private Date registerDate;

    private String uid;

    private String password;

    public User() {
    }

    public User(String firstName, String lastName, String gender, String email, String phone, String photo, String status, Date registerDate, String uid, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.photo = photo;
        this.status = status;
        this.registerDate = registerDate;
        this.uid = uid;
        this.password = password;
    }

    public List<Auction> getAuctions() {
        return auctions;
    }

    public void setAuctions(List<Auction> auctions) {
        this.auctions = auctions;
    }

    public List<UserCreditHistory> getUserCreditHistories() {
        return userCreditHistories;
    }

    public void setUserCreditHistories(List<UserCreditHistory> userCreditHistories) {
        this.userCreditHistories = userCreditHistories;
    }

    public List<WingTopUp> getWingTopUps() {
        return wingTopUps;
    }

    public void setWingTopUps(List<WingTopUp> wingTopUps) {
        this.wingTopUps = wingTopUps;
    }

    public List<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<Invoice> invoices) {
        this.invoices = invoices;
    }

    public List<Invoice> getInvoicePayees() {
        return invoicePayees;
    }

    public void setInvoicePayees(List<Invoice> invoicePayees) {
        this.invoicePayees = invoicePayees;
    }

    public List<BidHistory> getBidHistoryList() {
        return bidHistoryList;
    }

    public void setBidHistoryList(List<BidHistory> bidHistoryList) {
        this.bidHistoryList = bidHistoryList;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
//                ", role=" + this.getRole() +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender='" + gender + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", photo='" + photo + '\'' +
                ", status='" + status + '\'' +
                ", registerDate=" + registerDate +
                ", uid='" + uid + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
