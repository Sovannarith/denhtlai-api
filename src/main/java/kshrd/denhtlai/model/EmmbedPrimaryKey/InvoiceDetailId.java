package kshrd.denhtlai.model.EmmbedPrimaryKey;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class InvoiceDetailId implements Serializable{

//    @Column(name = "invoice_id")
    private Integer invoiceId;

//    @Column(name = "auction_id")
    private Integer auctionId;

    public Integer getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Integer getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(Integer auctionId) {
        this.auctionId = auctionId;
    }
}
