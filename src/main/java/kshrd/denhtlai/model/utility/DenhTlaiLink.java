package kshrd.denhtlai.model.utility;

import io.swagger.models.auth.In;
import kshrd.denhtlai.controller.rest.auction.AuctionRestController;
import kshrd.denhtlai.controller.rest.auctionType.AuctionTypeRestController;
import kshrd.denhtlai.controller.rest.category.CategoryRestController;
import kshrd.denhtlai.controller.rest.paymentMethod.PaymentMethodRestController;
import kshrd.denhtlai.controller.rest.product.ProductRestController;
import kshrd.denhtlai.controller.rest.role.RoleRestController;
import kshrd.denhtlai.controller.rest.user.UserRestController;
import kshrd.denhtlai.controller.rest.views.FindAllAuctionRestController;
import kshrd.denhtlai.model.*;
import kshrd.denhtlai.model.views.*;
import org.springframework.hateoas.Link;


import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;


public class DenhTlaiLink {

    /**
     * create static methods for reusable
     *
     * @return
     */
    private static Link userLink(Integer userId, String rel) {
        return linkTo(methodOn(UserRestController.class).findById(userId)).withRel(rel);
    }
    private static Link roleLink(Integer roleId){
        return linkTo(methodOn(RoleRestController.class).findById(roleId)).withRel("Role");
    }
    private static Link paymentMethodLink(Integer paymentMethodId){
        return linkTo(methodOn(PaymentMethodRestController.class).getPaymentMethodById(paymentMethodId)).withRel("PaymentMethod");
    }
    private static Link categoryLink(Integer categoryId, String rel){
        return linkTo(methodOn(CategoryRestController.class).findByCategoryId(categoryId)).withRel(rel);
    }
    private static Link auctionTypeLink(Integer auctionTypeId){
        return linkTo(methodOn(AuctionTypeRestController.class).findById(auctionTypeId)).withRel("AuctionType");
    }
    private static Link auctionLink(Integer auctionId){
        return linkTo(methodOn(AuctionRestController.class).findAuctionById(auctionId)).withRel("Auction");
    }
    private static Link findAllAuctionsLink(Integer findAllAuctionsId){
        return linkTo(methodOn(FindAllAuctionRestController.class).findByAuctionId(findAllAuctionsId)).withSelfRel();
    }
    private static Link productLink(Integer productId){
        return linkTo(methodOn(ProductRestController.class).findById(productId)).withRel("Product");
    }
    //************************************************************

    public static void setUserLink(User user){
        if(user == null) return;
        try{user.add(roleLink(user.getRole().getRoleId()));}catch (NullPointerException e){}
        user.add(userLink(user.getUserId(), "Self"));
    }

    public static void setCategoryLink(Category category){
        if(category == null) return;
        category.add(categoryLink(category.getCategoryId(), "Category"));
        if(category.getParentId() != null)
            category.add(categoryLink(category.getParentId().getCategoryId(), "Parent Category"));
    }

    public static void setAuctionLink(Auction auction){
        if (auction == null) return;
            auction.add(auctionLink(auction.getAuctionId()));
            if(auction.getAuctionType() != null) {
                auction.add(auctionTypeLink(auction.getAuctionType().getAuctionTypeId()));
            }
            if(auction.getCategory() != null) {
                auction.add(categoryLink(auction.getCategory().getCategoryId(), "Category"));
            }
            if(auction.getUser() != null) {
                auction.add(userLink(auction.getUser().getUserId(), "User"));
            }
    }

    public static void setFindAllAuctionsLink(FindAllAuctions findAllAuctions){
        if (findAllAuctions == null) return;
            if(findAllAuctions.getCategoryId() != null) {
                findAllAuctions.add(categoryLink(findAllAuctions.getCategoryId(), "Category"));
            }
        findAllAuctions.add(findAllAuctionsLink(findAllAuctions.getAuctionId()));
    }

    public static void setPaymentMethodLink(PaymentMethod paymentMethod){
        if(paymentMethod == null) return;
            paymentMethod.add(paymentMethodLink(paymentMethod.getPaymentMethodId()));
    }

    public static void setTopUpLink(TopUp topUp){
        if(topUp == null) return;
        if(topUp.getPaymentMethod() != null){
            topUp.add(paymentMethodLink(topUp.getPaymentMethod().getPaymentMethodId()));
            topUp.add(userLink(topUp.getUser().getUserId(), "User"));
            if(topUp.getUser().getRole() != null)
                topUp.add(roleLink(topUp.getUser().getRole().getRoleId()));
        }
    }
    /**
     * Use in FindWinAuctionRestController
     */
    public static void setFindWinAuctionLink(FindWinAuction findWinAuctionLink){
        if(findWinAuctionLink == null) return;
        if(findWinAuctionLink.getAuctionId() != null)
            findWinAuctionLink.add(auctionLink(findWinAuctionLink.getAuctionId()));
        if(findWinAuctionLink.getUserId() != null)
            findWinAuctionLink.add(userLink(findWinAuctionLink.getUserId(), "User"));
    }
    /**
     * Use in FindLostAuctionRestController
     */
    public static void setFindLostAuctionLink(FindLostAuction findLostAuction){
        if(findLostAuction == null) return;
        if(findLostAuction.getUserId() != null)
            findLostAuction.add(userLink(findLostAuction.getUserId(), "User"));
        if(findLostAuction.getwUserId() != null)
            findLostAuction.add(userLink(findLostAuction.getwUserId(), "Winner"));
        if(findLostAuction.getwAuctionId() != null)
            findLostAuction.add(auctionLink(findLostAuction.getwAuctionId()));
    }
    /**
     * Use in FindBidHistoryRestController
     */
    public static void setFindAllBidHistoryLink(FindAllBidHistory findAllBidHistory){
        if (findAllBidHistory == null) return;
        findAllBidHistory.add(auctionLink(findAllBidHistory.getAuctionId()));
    }

    public static void setFindAllBidWinnerLink(FindAllBidWinner findAllBidWinner) {
        if(findAllBidWinner == null) return;
            findAllBidWinner.add(auctionLink(findAllBidWinner.getAuctionId()));
    }

    public static void setProductLink(Product product){
        if (product == null) return;
            product.add(productLink(product.getProId()));

    }
}