package kshrd.denhtlai.model.views;

import org.springframework.hateoas.ResourceSupport;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "v_find_all_bid_histories")
public class FindAllBidHistory extends ResourceSupport implements Serializable {

    @Id
    @Column(name = "bid_id")
    private Integer bidId;

    @Column(name = "auction_id")
    private Integer auctionId;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "bid_price")
    private double bidPrice;

    @Column(name = "bid_date")
    @Temporal(TemporalType.DATE)
    private Date bidDate;

    public FindAllBidHistory() {
    }

    public FindAllBidHistory(Integer bidId, Integer auctionId, String productName, String firstName, String lastName, double bidPrice, Date bidDate) {
        this.bidId = bidId;
        this.auctionId = auctionId;
        this.productName = productName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.bidPrice = bidPrice;
        this.bidDate = bidDate;
    }

    public Integer getBidId() {
        return bidId;
    }

    public void setBidId(Integer bidId) {
        this.bidId = bidId;
    }

    public Integer getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(Integer auctionId) {
        this.auctionId = auctionId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(double bidPrice) {
        this.bidPrice = bidPrice;
    }

    public Date getBidDate() {
        return bidDate;
    }

    public void setBidDate(Date bidDate) {
        this.bidDate = bidDate;
    }
}
