package kshrd.denhtlai.model.views;

import org.springframework.hateoas.ResourceSupport;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "v_find_all_auctions")
public class FindAllAuctions extends ResourceSupport implements Serializable{

    @Id
    @Column(name = "auction_id")
    private Integer auctionId;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "description", length = 500)
    private String description;

    @Column(name = "category_id")
    private Integer categoryId;

    @Column(name = "category_name")
    private String categoryName;

    @Column(name = "start_price")
    private double startPrice;

    @Column(name = "current_price")
    private double currentPrice;

    @Column(name = "increment_price")
    private double incrementPrice;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "status", length = 10)
    private String status;

    @Column(name = "total_bid")
    private Integer totalBid;

    @Column(name = "buy_price")
    private double buyPrice;

    @Column(name = "image_path", length = 500)
    private String imagePath;

    public FindAllAuctions() {
    }

    public FindAllAuctions(Integer auctionId, String productName, String description, Integer categoryId, String categoryName, double startPrice, double currentPrice, double incrementPrice, Date startDate, Date endDate, String status, Integer totalBid, double buyPrice, String imagePath) {
        this.auctionId = auctionId;
        this.productName = productName;
        this.description = description;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.startPrice = startPrice;
        this.currentPrice = currentPrice;
        this.incrementPrice = incrementPrice;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
        this.totalBid = totalBid;
        this.buyPrice = buyPrice;
        this.imagePath = imagePath;
    }

    public Integer getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(Integer auctionId) {
        this.auctionId = auctionId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public double getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(double startPrice) {
        this.startPrice = startPrice;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public double getincrementPrice() {
        return incrementPrice;
    }

    public void setincrementPrice(double increasementPrice) {
        this.incrementPrice = incrementPrice;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTotalBid() {
        return totalBid;
    }

    public void setTotalBid(Integer totalBid) {
        this.totalBid = totalBid;
    }

    public double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(double buyPrice) {
        this.buyPrice = buyPrice;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    @Override
    public String toString() {
        return "FindAllAuctions{" +
                "auctionId=" + auctionId +
                ", productName='" + productName + '\'' +
                ", description='" + description + '\'' +
                ", categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                ", startPrice=" + startPrice +
                ", currentPrice=" + currentPrice +
                ", increasePrice=" + incrementPrice +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", status='" + status + '\'' +
                ", totalBid=" + totalBid +
                ", buyPrice=" + buyPrice +
                ", imagePath='" + imagePath + '\'' +
                '}';
    }
}

