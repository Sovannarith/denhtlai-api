package kshrd.denhtlai.model.views;

import org.springframework.hateoas.ResourceSupport;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "v_find_all_bid_winner")
public class FindAllBidWinner extends ResourceSupport implements Serializable {

    @Id
    @Column(name = "auction_id")
    private Integer auctionId;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    @Column(name = "current_price")
    private double currentPrice;

    @Column(name = "bid_date")
    @Temporal(TemporalType.DATE)
    private Date bidDate;

    public FindAllBidWinner() {
    }

    public FindAllBidWinner(Integer auctionId, String productName, String firstName, String lastName, String phone, String email, double currentPrice, Date bidDate) {
        this.auctionId = auctionId;
        this.productName = productName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.email = email;
        this.currentPrice = currentPrice;
        this.bidDate = bidDate;
    }

    public Integer getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(Integer auctionId) {
        this.auctionId = auctionId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public Date getBidDate() {
        return bidDate;
    }

    public void setBidDate(Date bidDate) {
        this.bidDate = bidDate;
    }

    @Override
    public String toString() {
        return "FindAllBidWinner = [ " +
                "auctionId=" + auctionId +
                ", productName='" + productName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", currentPrice=" + currentPrice +
                ", bidDate=" + bidDate +
                " ]";
    }
}
