package kshrd.denhtlai.model.views;

import org.springframework.hateoas.ResourceSupport;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "v_winner_with_status")
public class FindWinAuction extends ResourceSupport implements Serializable{

    @Id
    @Column(name = "auction_id")
    private Integer auctionId;

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "bid_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date bidDate;

    @Column(name = "current_price")
    private double currentPrice;

    @Column(name = "status", length = 10)
    private String status;

    public FindWinAuction(Integer auctionId, Integer userId, String productName, Date bidDate, double currentPrice, String status) {
        this.auctionId = auctionId;
        this.userId = userId;
        this.productName = productName;
        this.bidDate = bidDate;
        this.currentPrice = currentPrice;
        this.status = status;
    }

    public Integer getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(Integer auctionId) {
        this.auctionId = auctionId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Date getBidDate() {
        return bidDate;
    }

    public void setBidDate(Date bidDate) {
        this.bidDate = bidDate;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public FindWinAuction() {

    }
}
