package kshrd.denhtlai.model.views;

import org.springframework.hateoas.ResourceSupport;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "v_find_lost_auction")
public class FindLostAuction extends ResourceSupport implements Serializable{

    @Id
    @Column(name = "w_bid_id")
    private Integer wBidId;

    @Column(name = "w_user_id")
    private Integer wUserId;

    @Column(name = "w_auction_id")
    private Integer wAuctionId;

    @Column(name = "w_price")
    private double wPrice;

    @Column(name = "w_bid_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date wBidDate;

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "lose_price")
    private double losePrice;

    @Column(name = "product_name", length = 500)
    private String productName;

    public FindLostAuction() {
    }

    public FindLostAuction(Integer wBidId, Integer wUserId, Integer wAuctionId, double wPrice, Date wBidDate, Integer userId, double losePrice, String productName) {
        this.wBidId = wBidId;
        this.wUserId = wUserId;
        this.wAuctionId = wAuctionId;
        this.wPrice = wPrice;
        this.wBidDate = wBidDate;
        this.userId = userId;
        this.losePrice = losePrice;
        this.productName = productName;
    }

    public Integer getwBidId() {
        return wBidId;
    }

    public void setwBidId(Integer wBidId) {
        this.wBidId = wBidId;
    }

    public Integer getwUserId() {
        return wUserId;
    }

    public void setwUserId(Integer wUserId) {
        this.wUserId = wUserId;
    }

    public Integer getwAuctionId() {
        return wAuctionId;
    }

    public void setwAuctionId(Integer wAuctionId) {
        this.wAuctionId = wAuctionId;
    }

    public double getwPrice() {
        return wPrice;
    }

    public void setwPrice(double wPrice) {
        this.wPrice = wPrice;
    }

    public Date getwBidDate() {
        return wBidDate;
    }

    public void setwBidDate(Date wBidDate) {
        this.wBidDate = wBidDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public double getLosePrice() {
        return losePrice;
    }

    public void setLosePrice(double losePrice) {
        this.losePrice = losePrice;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public String toString() {
        return "FindLostAuction{" +
                "wBidId=" + wBidId +
                ", wUserId=" + wUserId +
                ", wAuctionId='" + wAuctionId + '\'' +
                ", wPrice=" + wPrice +
                ", wBidDate=" + wBidDate +
                ", userId=" + userId +
                ", losePrice=" + losePrice +
                ", productName='" + productName + '\'' +
                '}';
    }
}

