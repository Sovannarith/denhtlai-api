package kshrd.denhtlai.model;

import kshrd.denhtlai.model.EmmbedPrimaryKey.InvoiceDetailId;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "dt_invoice_details")
public class InvoiceDetail extends ResourceSupport implements Serializable {

    @EmbeddedId
    private InvoiceDetailId invoiceDetailId = new InvoiceDetailId();

    @ManyToOne
    @MapsId("invoiceId")
    @JoinColumn(name = "invoice_id")
    private Invoice invoice;

    @ManyToOne
    @MapsId("auctionId")
    @JoinColumn(name = "auction_id")
    private Auction auction;

    @Column(name = "price")
    private double price;

    public InvoiceDetail() {    }


    public InvoiceDetail(Invoice invoice, Auction auction, double price) {
        this.invoice = invoice;
        this.auction = auction;
        this.price = price;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "InvoiceDetail{" +
                "invoice=" + invoice +
                ", auction=" + auction +
                ", price=" + price +
                '}';
    }
}
