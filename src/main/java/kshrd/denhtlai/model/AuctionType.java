package kshrd.denhtlai.model;


import org.springframework.hateoas.ResourceSupport;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "dt_auction_types")
public class AuctionType extends ResourceSupport implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "auction_type_id")
    private Integer auctionTypeId;

    @Column(name = "auction_type")
    private String auctionType;

    @OneToMany(mappedBy = "auctionType", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Auction> auctions;

    public AuctionType() {
    }

    public AuctionType(String auctionType) {
        this.auctionType = auctionType;
    }

    public Integer getAuctionTypeId() {
        return auctionTypeId;
    }

    public void setAuctionTypeId(Integer auctionTypeId) {
        this.auctionTypeId = auctionTypeId;
    }

    public String getAuctionType() {
        return auctionType;
    }

    public void setAuctionType(String auctionType) {
        this.auctionType = auctionType;
    }

    @Override
    public String toString() {
        return "AuctionType{" +
                "auctionTypeId=" + auctionTypeId +
                ", auctionType='" + auctionType + '\'' +
                '}';
    }
}
