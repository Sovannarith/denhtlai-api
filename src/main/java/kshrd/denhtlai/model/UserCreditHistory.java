package kshrd.denhtlai.model;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "dt_user_credit_histories")
public class UserCreditHistory extends ResourceSupport implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "credit_id")
    private Integer creditId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public UserCreditHistory(User user, Date createDate, double beginningAmount, String transactionType, double amount, double endingAmount) {
        this.user = user;
        this.createDate = createDate;
        this.beginningAmount = beginningAmount;
        this.transactionType = transactionType;
        this.amount = amount;
        this.endingAmount = endingAmount;
    }

    public UserCreditHistory() {
    }

    @CreatedDate
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "beginning_amount")
    private double beginningAmount;

    @Column(name = "transaction_type", length = 50)
    private String transactionType;

    @Column(name = "amount")
    private double amount;

    @Column(name = "ending_amount")
    private double endingAmount;

    public Integer getCreditId() {
        return creditId;
    }

    public void setCreditId(Integer creditId) {
        this.creditId = creditId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public double getBeginningAmount() {
        return beginningAmount;
    }

    public void setBeginningAmount(double beginningAmount) {
        this.beginningAmount = beginningAmount;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getEndingAmount() {
        return endingAmount;
    }

    public void setEndingAmount(double endingAmount) {
        this.endingAmount = endingAmount;
    }
}
