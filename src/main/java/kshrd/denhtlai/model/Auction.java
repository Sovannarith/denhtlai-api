package kshrd.denhtlai.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "dt_auctions")
@EntityListeners(AuditingEntityListener.class)
public class Auction extends ResourceSupport implements Serializable{

    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "auction_id", insertable = false, updatable = false)
    private Integer auctionId;

//    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

//    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "auction_type_id")
    private AuctionType auctionType;

//    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "create_by_user_id")
    private User user;

    @JsonIgnore
    @OneToMany(mappedBy = "auction", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<BidHistory> bidHistoryList;

    @JsonIgnore
    @OneToMany(mappedBy = "auction", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<InvoiceDetail> invoiceDetails;

    @JsonIgnore
    @OneToMany(mappedBy = "auction", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Gallery> galleries;

    public Auction() {    }

    public Auction(Category category, AuctionType auctionType, User user, List<BidHistory> bidHistoryList, List<InvoiceDetail> invoiceDetails, List<Gallery> galleries, String productName, String description, double startPrice, double buyPrice, double currentPrice, double incrementPrice, Date startDate, Date endDate, String status, Timestamp createDate, Date lastModifiedDate, Integer winnerId) {
        this.category = category;
        this.auctionType = auctionType;
        this.user = user;
        this.bidHistoryList = bidHistoryList;
        this.invoiceDetails = invoiceDetails;
        this.galleries = galleries;
        this.productName = productName;
        this.description = description;
        this.startPrice = startPrice;
        this.buyPrice = buyPrice;
        this.currentPrice = currentPrice;
        this.incrementPrice = incrementPrice;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
        this.createDate = createDate;
        this.lastModifiedDate = lastModifiedDate;
        this.winnerId = winnerId;
    }

    @Column(name = "product_name")
    private String productName;

    @Column(name = "description", length = 500)
    private String description;

    @Column(name = "start_price")
    private double startPrice;

    @Column(name = "buy_price")
    private double buyPrice;

    @Column(name = "current_price")
    private double currentPrice;

    @Column(name = "increment_price")
    private double incrementPrice;

    @Column(name = "start_date")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date startDate;

    @Column(name = "end_date")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date endDate;

    @Column(name = "status")
    private String status;

    @CreatedDate
    @Column(name = "create_date", nullable = false, updatable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date createDate;

    @LastModifiedDate
    @Column(name = "last_modifed_date")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @Column(name = "winner_id")
    private Integer winnerId;

    public Integer getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(Integer auctionId) {
        this.auctionId = auctionId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public AuctionType getAuctionType() {
        return auctionType;
    }

    public void setAuctionType(AuctionType auctionType) {
        this.auctionType = auctionType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<BidHistory> getBidHistoryList() {
        return bidHistoryList;
    }

    public void setBidHistoryList(List<BidHistory> bidHistoryList) {
        this.bidHistoryList = bidHistoryList;
    }

    public List<InvoiceDetail> getInvoiceDetails() {
        return invoiceDetails;
    }

    public void setInvoiceDetails(List<InvoiceDetail> invoiceDetails) {
        this.invoiceDetails = invoiceDetails;
    }

    public List<Gallery> getGalleries() {
        return galleries;
    }

    public void setGalleries(List<Gallery> galleries) {
        this.galleries = galleries;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(double startPrice) {
        this.startPrice = startPrice;
    }

    public double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(double buyPrice) {
        this.buyPrice = buyPrice;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public double getIncrementPrice() {
        return incrementPrice;
    }

    public void setIncrementPrice(double incrementPrice) {
        this.incrementPrice = incrementPrice;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Integer getWinnerId() {
        return winnerId;
    }

    public void setWinnerId(Integer winnerId) {
        this.winnerId = winnerId;
    }

    @Override
    public String toString() {
        return "Auction{" +
                "auctionId=" + auctionId +
                ", category=" + category +
                ", auctionType=" + auctionType +
                ", user=" + user +
                ", bidHistoryList=" + bidHistoryList +
                ", invoiceDetails=" + invoiceDetails +
                ", galleries=" + galleries +
                ", productName='" + productName + '\'' +
                ", description='" + description + '\'' +
                ", startPrice=" + startPrice +
                ", buyPrice=" + buyPrice +
                ", currentPrice=" + currentPrice +
                ", incrementPrice=" + incrementPrice +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", status='" + status + '\'' +
                ", createDate=" + createDate +
                ", lastModifiedDate=" + lastModifiedDate +
                ", winnerId=" + winnerId +
                '}';
    }
}
