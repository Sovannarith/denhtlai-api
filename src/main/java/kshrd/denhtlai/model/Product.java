package kshrd.denhtlai.model;

import org.springframework.hateoas.ResourceSupport;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "dt_products")
public class Product extends ResourceSupport implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "pro_id")
    private Integer proId;

    @Column(name = "pro_name")
    private String name;

    public Product() {    }

    public Product(String name) {
        this.name = name;
    }

    public Integer getProId() {
        return proId;
    }

    public void setProId(Integer proId) {
        this.proId = proId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Product{" +
                "proId=" + proId +
                ", name='" + name + '\'' +
                '}';
    }
}
