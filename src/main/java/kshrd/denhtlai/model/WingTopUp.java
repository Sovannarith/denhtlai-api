package kshrd.denhtlai.model;


import org.springframework.hateoas.ResourceSupport;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "dt_wing_topups")
public class WingTopUp extends ResourceSupport implements Serializable{

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "wing_topup_id")
    private Integer wingTopUpId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public WingTopUp() {    }

    public WingTopUp(User user, double amount, String sender, String type, String code, String status, Date wingTopupDate, String currency) {
        this.user = user;
        this.amount = amount;
        this.sender = sender;
        this.type = type;
        this.code = code;
        this.status = status;
        this.wingTopupDate = wingTopupDate;
        this.currency = currency;
    }

    @Column(name = "amount")
    private double amount;

    @Column(name = "sender", length = 50)
    private String sender;

    @Column(name = "type", length = 50)
    private String type;

    @Column(name = "code", length = 50)
    private String code;

    @Column(name = "status", length = 20)
    private String status;

    @Column(name = "wing_topup_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date wingTopupDate;

    @Column(name = "currency")
    private String currency;

    public Integer getWingTopUpId() {
        return wingTopUpId;
    }

    public void setWingTopUpId(Integer wingTopUpId) {
        this.wingTopUpId = wingTopUpId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getWingTopupDate() {
        return wingTopupDate;
    }

    public void setWingTopupDate(Date wingTopupDate) {
        this.wingTopupDate = wingTopupDate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
