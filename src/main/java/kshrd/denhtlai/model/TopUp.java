package kshrd.denhtlai.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "dt_top_ups")
@EntityListeners(AuditingEntityListener.class)
public class TopUp extends ResourceSupport implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "top_up_id")
    private Integer topUpId;

    /**
     * Relationship with Table: dt_users
     * Relationship Type: ManyToOne
     */
//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
//    @JsonDeserialize
//    @JsonSerialize
//    @Transient
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    /**
     * Relationship with Table:  st_payment_methods
     * Relationship Type: ManyToOne
     */
    @ManyToOne
    @JoinColumn(name = "payment_method_id")
    private PaymentMethod paymentMethod;

    public TopUp() {    }

    public TopUp(User user, PaymentMethod paymentMethod, String currency, double amount, Date topUpDate, Integer adminId) {
        this.user = user;
        this.paymentMethod = paymentMethod;
        this.currency = currency;
        this.amount = amount;
        this.topUpDate = topUpDate;
        this.adminId = adminId;
    }

    @Column(name = "currency", length = 5)
    private String currency;

    @Column(name = "amount")
    private double amount;

    @CreatedDate
    @Column(name = "top_up_date", nullable = false, updatable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date topUpDate;

    @Column(name = "admin_id")
    private Integer adminId;

    public Integer getTopUpId() {
        return topUpId;
    }

    public void setTopUpId(Integer topUpId) {
        this.topUpId = topUpId;
    }

    @JsonProperty
    public User getUser() {
        return user;
    }

    @JsonIgnore
    public void setUser(User user) {
        this.user = user;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getTopUpDate() {
        return topUpDate;
    }

    public void setTopUpDate(Date topUpDate) {
        this.topUpDate = topUpDate;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    @Override
    public String toString() {
        return "TopUp{" +
                "topUpId=" + topUpId +
                ", user=" + user +
                ", paymentMethod=" + paymentMethod +
                ", currency='" + currency + '\'' +
                ", amount=" + amount +
                ", topUpDate=" + topUpDate +
                ", adminId=" + adminId +
                '}';
    }
}
