package kshrd.denhtlai.model;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "dt_bid_histories")
@EntityListeners(AuditingEntityListener.class)
public class BidHistory extends ResourceSupport implements Serializable {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bid_id")
    private Integer bidId;

    /**
     * Relationship with Table: dt_users
     * Relationship Type: ManyToOne
     */
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    /**
     * Relationship with Table: dt_auctions
     * Relationship Type: ManyToOne
     */
    @ManyToOne
    @JoinColumn(name = "auction_id")
    private Auction auction;

    public BidHistory() {    }

    public BidHistory(User user, Auction auction, double bidPrice, Date bidDate) {
        this.user = user;
        this.auction = auction;
        this.bidPrice = bidPrice;
        this.bidDate = bidDate;
    }

    @Column(name = "bid_price")
    private double bidPrice;

    @CreatedDate
    @Column(name = "bid_date", nullable = false, updatable = false)
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date bidDate;

    public Integer getBidId() {
        return bidId;
    }

    public void setBidId(Integer id) {
        this.bidId = bidId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Auction getAuction() {
        return auction;
    }

    public void setAuction(Auction auction) {
        this.auction = auction;
    }

    public double getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(double bidPrice) {
        this.bidPrice = bidPrice;
    }

    public Date getBidDate() {
        return bidDate;
    }

    public void setBidDate(Date bidDate) {
        this.bidDate = bidDate;
    }

    @Override
    public String toString() {
        return "BidHistory{" +
                "bidId=" + bidId +
                ", user=" + user +
                ", auction=" + auction +
                ", bidPrice=" + bidPrice +
                ", bidDate=" + bidDate +
                '}';
    }
}
