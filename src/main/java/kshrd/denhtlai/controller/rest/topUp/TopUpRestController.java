package kshrd.denhtlai.controller.rest.topUp;


import io.swagger.annotations.ApiOperation;
import kshrd.denhtlai.model.TopUp;
import kshrd.denhtlai.model.utility.DenhTlaiLink;
import kshrd.denhtlai.service.topUp.TopUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/topUps")
public class TopUpRestController {

    private TopUpService topUpService;

    @Autowired
    TopUpRestController(TopUpService topUpService){
        this.topUpService = topUpService;
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public TopUp save(@RequestBody TopUp topUp){
        topUp.setAdminId(null);
        TopUp topUp1 = topUpService.topUp(topUp);
        return topUp1;
    }

    @ApiOperation(value = "រក្សាទុក TopUp")
    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public TopUp save_(  @RequestParam Integer userId
                       , @RequestParam String currency
                       , @RequestParam double amount
                       , @RequestParam Integer payment_method){
        TopUp topUp1 = topUpService.topUp_(userId, currency, amount, payment_method);
        return topUp1;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<TopUp> findTopUps(@RequestParam(name = "pageName" , required = false) Integer pageNumber
                                , @RequestParam(name = "pageSize", required = false) Integer pageSize
                                , @RequestParam(name = "order", required = false) String order
                                , @RequestParam(name = "field", required = false) String field){
        Page<TopUp> topUpList = topUpService.findTopUps(pageNumber, pageSize, order, field);
        topUpList.forEach(topUp -> {
            DenhTlaiLink.setTopUpLink(topUp);
        });
        return topUpList;
    }

    @GetMapping(value = "/userId/{userId}")
    public Page<TopUp> findTopUpByUserId(@PathVariable Integer userId
                                        , @RequestParam(value = "pageName", required = false) Integer pageNumber
                                        , @RequestParam(value = "pageSize", required = false) Integer pageSize
                                        , @RequestParam(value = "order", required = false) String order
                                        , @RequestParam(value = "field", required = false) String field){
        Page<TopUp> topUps = topUpService.findByUserId(userId, pageNumber, pageSize, order, field);
        topUps.forEach(topUp -> {
            DenhTlaiLink.setTopUpLink(topUp);
        });
        return topUps;
    }

    @GetMapping(value = "/topUpId/{topUpId}")
    public Page<TopUp> findTopById( @PathVariable Integer topUpId
                                    , @RequestParam(value = "pageName", required = false) Integer pageNumber
                                    , @RequestParam(value = "pageSize", required = false) Integer pageSize
                                    , @RequestParam(value = "order", required = false) String order
                                    , @RequestParam(value = "field", required = false) String field){
        Page<TopUp> topUps = topUpService.findById(topUpId, pageNumber, pageSize, order, field);
        topUps.forEach(topUp -> {
            DenhTlaiLink.setTopUpLink(topUp);
        });
        return topUps;
    }

    @GetMapping(value = "/paymentMethodId/{paymentMethodId}")
    public Page<TopUp> findByPaymentMethod_PaymentMethodId (@PathVariable Integer paymentMethodId
                                                            , @RequestParam(value = "pageNumber", required = false) Integer pageNumber
                                                            , @RequestParam(value = "pageSize", required = false) Integer pageSize
                                                            , @RequestParam(value = "order", required = false) String order
                                                            , @RequestParam(value = "field", required = false) String field){
        Page<TopUp> topUps = topUpService.findByPaymentMethod_PaymentMethodId(paymentMethodId, pageNumber, pageSize, order, field);
        topUps.forEach(topUp -> {
            DenhTlaiLink.setTopUpLink(topUp);
        });
        return topUps;
    }

}
