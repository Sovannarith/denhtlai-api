package kshrd.denhtlai.controller.rest.role;


import kshrd.denhtlai.model.Role;
import kshrd.denhtlai.service.role.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/roles")
public class RoleRestController {

    private RoleService roleService;

    @Autowired
    public RoleRestController(RoleService roleService){
        this.roleService = roleService;
    }

    @GetMapping("/roleId/{roleId}")
    public Role findById(@PathVariable("roleId") Integer id){
        return roleService.findByRoleId(id);
    }


}
