package kshrd.denhtlai.controller.rest.auctionType;


import kshrd.denhtlai.controller.rest.auction.AuctionRestController;
import kshrd.denhtlai.model.Auction;
import kshrd.denhtlai.model.AuctionType;
import kshrd.denhtlai.model.utility.DenhTlaiLink;
import kshrd.denhtlai.service.auctionType.AuctionTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auctionTypes")
public class AuctionTypeRestController {

    private AuctionTypeService auctionTypeService;

    @Autowired
    public AuctionTypeRestController(AuctionTypeService auctionTypeService){
        this.auctionTypeService = auctionTypeService;
    }

    @GetMapping(value = "/auctionType/{auctionTypeId}")
    public AuctionType findById(@PathVariable("auctionTypeId") Integer id){
        AuctionType auctionType = auctionTypeService.findByAuctionTypeId(id);
        return auctionType;
    }

}
