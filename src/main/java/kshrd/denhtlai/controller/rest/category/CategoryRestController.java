package kshrd.denhtlai.controller.rest.category;

import kshrd.denhtlai.model.Category;
import kshrd.denhtlai.model.utility.DenhTlaiLink;
import kshrd.denhtlai.service.category.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/api/v1/categories")
public class CategoryRestController {

    private CategoryService categoryService;

    @Autowired
    public CategoryRestController(CategoryService categoryService){
        this.categoryService = categoryService;
    }

    @GetMapping(value = "")
    public Page<Category> findCategories( @RequestParam(required = false) Integer pageNumber
                                        , @RequestParam(required = false) Integer pageSize
                                        , @RequestParam(required = false) String order
                                        , @RequestParam(required = false) String field){
        Page<Category> categories = categoryService.findAll(pageNumber, pageSize, order, field);
        categories.forEach(category -> DenhTlaiLink.setCategoryLink(category));
        return categories;
    }

    @GetMapping(value = "/categoryId/{categoryId}")
    public Category findByCategoryId(  @PathVariable("categoryId") Integer id){
        return categoryService.findById(id);
    }

    @PostMapping(value = "")
    @Transactional
    public Category saveCategory(@RequestBody Category category){
        return categoryService.save(category);
    }

    @PutMapping(value = "/categoryId/{categoryId}")
    public Category updateCategory(@PathVariable("categoryId") Integer id, @RequestBody Category category){
        return categoryService.update(id, category);
    }

    @DeleteMapping(value = "")
    public Category deleteCategory(@RequestParam Integer id){
        return categoryService.delete(id);
    }


}
