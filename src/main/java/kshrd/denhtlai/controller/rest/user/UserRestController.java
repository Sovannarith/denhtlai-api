package kshrd.denhtlai.controller.rest.user;

import kshrd.denhtlai.model.User;
import kshrd.denhtlai.model.utility.DenhTlaiLink;
import kshrd.denhtlai.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/users")
public class UserRestController {

    UserService userService;


    @Autowired
    UserRestController(UserService userService){
        this.userService = userService;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<User> findAllUser(  @RequestParam(required = false) Integer pageNumber
                                  , @RequestParam(required = false) Integer pageSize
                                  , @RequestParam(required = false) String order
                                  , @RequestParam(required = false) String field){
        Page<User> page = userService.findAll(pageNumber, pageSize  , order, field);
        page.getContent().forEach(user -> DenhTlaiLink.setUserLink(user));
        return page;
    }
    @GetMapping(value="/sign-in", produces = MediaType.APPLICATION_JSON_VALUE)
    public User findByPhoneAndUid(@RequestParam String phone, @RequestParam String uid){
        User user = userService.findByPhoneAndUid(phone, uid);
        DenhTlaiLink.setUserLink(user);
        return user;
    }

    @RequestMapping( value = "/sign-up", method = RequestMethod.POST)
    public User save(@RequestBody User user){
            return  userService.save(user);
    }

    @GetMapping(value = "/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ExceptionHandler(value = Exception.class)
    public User findById(@PathVariable("userId") Integer id){
        return userService.findById(id);
    }

    @GetMapping(value = "/username/{name}")
    public Page<User> findByFirstNameIgnoreCaseContainsOrLastNameIgnoreCaseContains(
                                                     @PathVariable String name
                                                   , @RequestParam(required = false) Integer pageNumber
                                                   , @RequestParam(required = false) Integer pageSize
                                                   , @RequestParam(required = false) String order
                                                   , @RequestParam(required = false) String field){
        Page<User> page = userService.findByFirstNameIgnoreCaseContainsOrLastNameIgnoreCaseContains(name, name, pageNumber, pageSize  , order, field);
        page.forEach(user -> DenhTlaiLink.setUserLink(user));
        return page;
    }

    @PutMapping(value = "/{userId}")
    public User updateUser(@PathVariable("userId") Integer userId, @RequestBody User user){
        return userService.update(userId, user);
    }

//    @PutMapping(value = "/admin/{userId}")
//    public User adminUpdateUser(@PathVariable("userId") Integer userId, @RequestBody User user){
//        return userService.adminUpdate(userId, user);
//    }
// update status ey tov


}
