package kshrd.denhtlai.controller.rest.product;

import kshrd.denhtlai.model.Product;
import kshrd.denhtlai.model.utility.DenhTlaiLink;
import kshrd.denhtlai.service.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/products")
public class ProductRestController {

    private ProductService productService;

    @Autowired
    ProductRestController(ProductService productService){
        this.productService = productService;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<Product> findProducts(@RequestParam(required = false) Integer pageNumber
                                    , @RequestParam(required = false) Integer pageSize
                                    , @RequestParam(required = false) String order
                                    , @RequestParam(required = false) String field){
        Page<Product> findProducts = productService.findAll(pageNumber, pageSize, order, field);
        findProducts.forEach(product -> DenhTlaiLink.setProductLink(product));
        return findProducts;
    }
    @GetMapping(value = "/productId/{productId}")
    public Product findById(@PathVariable Integer productId){
        Product product = productService.findById(productId);
        DenhTlaiLink.setProductLink(product);
        return product;
    }

    @PostMapping(value = "")
    public Product saveProduct(@RequestBody Product product){
        return productService.save(product);
    }

    @PutMapping(value = "/productId/{productId}")
    public Product updateProduct(@PathVariable Integer productId, @RequestBody Product product){
        return productService.update(productId, product);
    }

    @DeleteMapping(value = "/productId/{productId}")
    public Product deleteProduct(@PathVariable Integer productId){
        return productService.delete(productId);
    }

}
