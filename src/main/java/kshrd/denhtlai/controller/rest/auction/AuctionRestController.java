package kshrd.denhtlai.controller.rest.auction;

import io.swagger.annotations.ApiOperation;
import kshrd.denhtlai.model.Auction;
import kshrd.denhtlai.model.utility.DenhTlaiLink;
import kshrd.denhtlai.service.auction.AuctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/auctions")
public class AuctionRestController {

    private AuctionService auctionService;

    @Autowired
    AuctionRestController(AuctionService auctionService){
        this.auctionService = auctionService;
    }

    @PostMapping("")
    public Auction save (@RequestBody Auction auction){

        return auctionService.save(auction);
    }

    @PutMapping("")
    public Auction update (@RequestBody Auction auction){
        return auctionService.update(auction);
    }

    @ApiOperation("បង្ហាញ Auction ទាំងអស់")
    @GetMapping("")
    public Page<Auction> findAll(@RequestParam(name = "pageIndex" , required = false) Integer pageNumber
            , @RequestParam(name = "pageSize", required = false) Integer pageSize
            , @RequestParam(name = "order", required = false) String order
            , @RequestParam(name = "field", required = false) String field){
        Page<Auction> auctionPage = auctionService.findAll(pageNumber, pageSize,order,field);
        auctionPage.getContent().forEach(auction -> {
            DenhTlaiLink.setAuctionLink(auction);
                }
        );
        return auctionPage;
    }


    @GetMapping("/productnames/{productName}")
    public Page<Auction> findAuctionsByProductNameContains(   @PathVariable(name = "productName", required = false) String productName
                                                            , @RequestParam(name = "pageIndex" , required = false) Integer pageNumber
                                                            , @RequestParam(name = "pageSize", required = false) Integer pageSize
                                                            , @RequestParam(name = "order", required = false) String order
                                                            , @RequestParam(name = "field", required = false) String field){
        Page<Auction> auctionPage = auctionService.findByProductNameIgnoreCaseContains(productName, pageNumber, pageSize, order, field);
        auctionPage.getContent().forEach(auction -> DenhTlaiLink.setAuctionLink(auction));
        return auctionPage;
    }

    @GetMapping("/auctionid/{auctionId}")
    public Auction findAuctionById(  @PathVariable("auctionId") Integer auctionId){
        Auction auction = auctionService.findById(auctionId);
        DenhTlaiLink.setAuctionLink(auction);
        return auction;
    }

    @GetMapping("/userid/{userId}")
    public Page<Auction> findByUser_UserId(  @PathVariable("userId") Integer userId
                                            , @RequestParam(name = "pageIndex", required = false) Integer pageNumber
                                            , @RequestParam(name = "pageSize", required = false) Integer pageSize
                                            , @RequestParam(name = "order", required = false) String order
                                            , @RequestParam(name = "field", required = false) String field){
        Page<Auction> auctionPage = auctionService.findByUser_UserId(userId, pageNumber, pageSize, order, field);
        auctionPage.getContent().forEach(auction -> DenhTlaiLink.setAuctionLink(auction));
        return auctionPage;
    }

}
