package kshrd.denhtlai.controller.rest.views;

import io.swagger.annotations.ApiOperation;
import kshrd.denhtlai.model.utility.DenhTlaiLink;
import kshrd.denhtlai.model.views.FindWinAuction;
import kshrd.denhtlai.service.views.FindWinAuctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/winAuctions")
public class FindWinAuctionRestController {

    private FindWinAuctionService findWinAuctionService;

    @Autowired
    public FindWinAuctionRestController (FindWinAuctionService findWinAuctionService){
        this.findWinAuctionService = findWinAuctionService;
    }

    @ApiOperation(value = "បង្ហាញ Auction ដែលអ្នកប្រើប្រាស់បានឈ្នះ")
    @GetMapping(value = "/userId/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<FindWinAuction> findByUserId(@PathVariable Integer userId,
                                             @RequestParam(required = false) Integer pageNumber
                                            , @RequestParam(required = false) Integer pageSize
                                            , @RequestParam(required = false) String order
                                            , @RequestParam(required = false) String field){

        Page<FindWinAuction> findWinAuctions = findWinAuctionService.findByUserId(userId, pageNumber, pageSize, order, field);
        findWinAuctions.forEach(findWinAuction -> DenhTlaiLink.setFindWinAuctionLink(findWinAuction));
        return findWinAuctions;
    }

}
