package kshrd.denhtlai.controller.rest.views;

import kshrd.denhtlai.model.utility.DenhTlaiLink;
import kshrd.denhtlai.model.views.FindLostAuction;
import kshrd.denhtlai.service.views.FindLostAuctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;
import java.util.List;

@RestController
@Transactional
@RequestMapping("/api/v1/lostAuctions")
public class FindLostAuctionRestController {

    private FindLostAuctionService findLostAuctionService;

    @Autowired
    FindLostAuctionRestController(FindLostAuctionService findLostAuctionService){
        this.findLostAuctionService = findLostAuctionService;
    }


    @GetMapping("/userId/{userId}")
    public Page<FindLostAuction> findLostAuctions(@PathVariable Integer userId,
                                                  @RequestParam(required = false) Integer pageNumber
                                                , @RequestParam(required = false) Integer pageSize
                                                , @RequestParam(required = false) String order
                                                , @RequestParam(required = false) String field){
        Page<FindLostAuction> list = findLostAuctionService.findByWUserIdNotAndUserIdIs(userId, pageNumber, pageSize, order, field);
        list.forEach(findLostAuction -> {
            DenhTlaiLink.setFindLostAuctionLink(findLostAuction);
        });
        return list;
    }

}
