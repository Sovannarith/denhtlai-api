package kshrd.denhtlai.controller.rest.views;


import io.swagger.models.auth.In;
import kshrd.denhtlai.model.utility.DenhTlaiLink;
import kshrd.denhtlai.model.views.FindAllAuctions;
import kshrd.denhtlai.service.views.FindAllAuctionService;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/vauctions")
public class FindAllAuctionRestController {

    private FindAllAuctionService findAllAuctionService;

    FindAllAuctionRestController(FindAllAuctionService findAllAuctionService){
        this.findAllAuctionService = findAllAuctionService;
    }

    @GetMapping("")
    public Page<FindAllAuctions> findAllAuctions(@RequestParam(required = false) Integer pageNumber
                                                , @RequestParam(required = false) Integer pageSize
                                                , @RequestParam(required = false) String order
                                                , @RequestParam(required = false) String field){
        Page<FindAllAuctions> allAuctions = findAllAuctionService.findAll(pageNumber, pageSize, order, field);
        allAuctions.getContent().forEach(findAllAuctions -> DenhTlaiLink.setFindAllAuctionsLink(findAllAuctions));
        return allAuctions;
    }

    @GetMapping("/auctionId/{auctionId}")
    public FindAllAuctions findByAuctionId(@PathVariable Integer auctionId){
        return findAllAuctionService.findByAuctionId(auctionId);
    }
}
