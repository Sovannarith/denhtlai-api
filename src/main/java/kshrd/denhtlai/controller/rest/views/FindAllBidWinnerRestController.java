package kshrd.denhtlai.controller.rest.views;


import kshrd.denhtlai.model.utility.DenhTlaiLink;
import kshrd.denhtlai.model.views.FindAllBidWinner;
import kshrd.denhtlai.service.views.FindAllBidWinnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/v1/winners")
public class FindAllBidWinnerRestController {

    FindAllBidWinnerService findAllBidWinnerService;

    @Autowired
    FindAllBidWinnerRestController(FindAllBidWinnerService findAllBidWinnerService){
        this.findAllBidWinnerService = findAllBidWinnerService;
    }

    @GetMapping(value = "")
    public Page<FindAllBidWinner> findAllBinWinners(@RequestParam(required = false) Integer pageNumber
                                                    , @RequestParam(required = false) Integer pageSize
                                                    , @RequestParam(required = false) String order
                                                    , @RequestParam(required = false) String field){
        Page<FindAllBidWinner> findAllBidWinners = findAllBidWinnerService.findAll(pageNumber, pageSize, order, field);
        findAllBidWinners.forEach(findAllBidWinner -> DenhTlaiLink.setFindAllBidWinnerLink(findAllBidWinner));
        return findAllBidWinners;
    }

}
