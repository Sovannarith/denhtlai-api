package kshrd.denhtlai.controller.rest.views;

import kshrd.denhtlai.model.utility.DenhTlaiLink;
import kshrd.denhtlai.model.views.FindAllBidHistory;
import kshrd.denhtlai.service.views.FindAllBidHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/bidHistorys")
public class FindAllBidHistoryRestController {

    private FindAllBidHistoryService findAllBidHistoryService;

    @Autowired
    FindAllBidHistoryRestController(FindAllBidHistoryService findAllBidHistoryService){
        this.findAllBidHistoryService = findAllBidHistoryService;
    }

    @GetMapping("")
    public Page<FindAllBidHistory> findAllBidHistories(@RequestParam(required = false) Integer pageNumber
                                                        , @RequestParam(required = false) Integer pageSize
                                                        , @RequestParam(required = false) String order
                                                        , @RequestParam(required = false) String field){
        Page<FindAllBidHistory> findAllBidHistories = findAllBidHistoryService.findAll(pageNumber, pageSize, order, field);
        findAllBidHistories.forEach(findAllBidHistory -> DenhTlaiLink.setFindAllBidHistoryLink(findAllBidHistory));
        return findAllBidHistories;

    }


}
