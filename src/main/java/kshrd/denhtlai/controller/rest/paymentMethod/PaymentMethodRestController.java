package kshrd.denhtlai.controller.rest.paymentMethod;

import kshrd.denhtlai.model.PaymentMethod;
import kshrd.denhtlai.model.utility.DenhTlaiLink;
import kshrd.denhtlai.service.paymentMethod.PaymentMethodService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/paymentMethods")
public class PaymentMethodRestController {

    private PaymentMethodService paymentMethodService;

    public PaymentMethodRestController(PaymentMethodService paymentMethodService){
        this.paymentMethodService = paymentMethodService;
    }

    @GetMapping(value = "/paymentMethodId/{paymentMethodId}")
    public PaymentMethod getPaymentMethodById(@PathVariable Integer paymentMethodId){
        PaymentMethod paymentMethod = paymentMethodService.getPaymentMethodById(paymentMethodId);
        DenhTlaiLink.setPaymentMethodLink(paymentMethod);
        return paymentMethod;
    }

}
