package kshrd.denhtlai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class DenhtlaiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DenhtlaiApplication.class, args);
	}
}
